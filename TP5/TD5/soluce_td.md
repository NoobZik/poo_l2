# TD de programmation Orientée Objet #

## Rappel de la semaine derniere ##

Definition des classes

| Objet Zork     | Piece                      |
| :------------- | :------------------------- |
| Description    | Liste des objet\[nbmax]   |
| Transportable  | nbObjet                    |
| Poids					 | nbMax                      |
| position       |                            |

Liste des objet qui le compose :

*   Ajouter
*   Retirer
*   Contient
*   Contient\[CombienDe]

## Utilisation des tableaux et test d'égalité ##

### Exercice 1 ###

1)
| equals(); 									| opérateur "=="     														 |
| :-------------------------- | :--------------------------------------------- |
| Teste l'égalité des contenu | Teste si les 2 variables ont la même reference |
|	Redéfinir equals() selon les objets a comparer | Teste si les 2 variables ont le même objet     |

```java
String chaine1 = "coucou";
String chaine2 = "cou";

chaine2 = chaine2 + chaine2;
if(chaine1 == chaine2)
	System.out.println("identique");
else
	System.out.println("pas identique");
```

Le code va afficher :
```bash!
Pas identique
```

2)
Il suffit juste de remplacer chaine1 == chaine2

```java
chaine1.equals(chaine2);
```

*Pour la troisième question, voir le tableau de la question 1*

4)

```java
private class ObjetZork{
	private String 	description;
	private int 	 	poids;
	private boolean transportable;

	// Constructeurs
	// Accesseurs

	public boolean equals(Object o){
		if(!(o instanceof ObjetZork))
			return false;
		ObjetZork oz = (ObjetZork) o;
		if(this.getDescription().equals(oz.getDescription()) &&
			this.getPoids()==oz.getPoids() && this.estTransportable() == oz.estTransportable())
			return true;
	}
}
```

## Exercice 2 ##

```java
public class Piece{
	private ObjetZork[] listeObjet;
	private int nbObjet;
	private int nbMaxObjet;

	// Constructeurs
	// Accesseurs

	public int contientCombienDe(ObjetZork o){}
	public void ajouter (ObjetZork o) {}
	public boolean retirer (ObjetZork o) {}
	public boolean contient(ObjetZork o) {}
}
```
```java
public int contientCombienDe(OjetZork o) {
	int i = -1;
	int n = 0;
	while(++i<getNbObjet()){
		if(listeObjet[i].equals(o)) n++;
		else continue;
	}
	return n;
}

public void ajouter(ObjetZork o){
	if(this.getNbObjet() < this.getNbMaxObjet())
		listeObjet[nbObjet+1] = 0;
		nbObjets++;
}

public boolean retirer (ObjetZork o){
	int i = -1;
	while(++i < nbMaxObjet){
		if(listeObjet[i].equals(o)){
			listeObjet[i] = listeObjet[nbObjets-1];
			nbObjets--;
			listeObjet[nbObjets] = null;
		}
		return true;
	}
	return false;
}
```
