## Exercice 1 ##

Dans quel context ou ces classes seront manipulés ?
Arborecense ? Une relation d'héritage entre classe.

1.  context : Relation "est_un" du sens commun (exemple : un velib est vélo est
  un vélo est un deux roues). Cependant d'un point de vue de conception objet,
  cette hierarchie être précisé en prenant compte des differences entre les
  caractéristiques d'un velib (ex: un velib est un vélo qu'on peut louer, un
  vélo moteur, est une moto, dont le cylindre et la vitesse est limité).

2.  context : Si on se place dans un context particulier cette hierarchie peut être
  remise en cause.

Par exemple : s'il s'agit d'afficher les véhicules selon leur position GPS
  transmise par les véhicules de la circulation réelle, on a donc seulement
  besoin de connaitre le pictogramme à afficher et la position GPS du
  véhicule. --> une seul classe suffirait pour mettre en relation tous ces objets
  (velo, velib, voiture,...)

3.  context: Simulation de flux de véhicules : dans ce cas les positions des
véhicules peuvent être determinée en fonction de leur voiture, leur posibilité
d'acceleration..

4.  context: Simulation du nombre de personnes transportés.

5.  context: Simulation des emissions de CO2 de chaque véhicules.

Conlusion : On voit que dans chacun des cas précédents selon le context on a
trouvé une hiéarchie correspondante.

```java
/**
 * Le point spécifier doit être non null
 * La valeur spécifié pour le coté doit etre strictement positive
 *
 * @requires coinSupGauche != null
 * @requires cote  > 0
 * @ensres getCoinSupGauche().equals(coinSupGauche);
 * @param  coinSupGauche le coin supérieur gauche du nouveau carré
 * @param cote le cote du carre
 * @throws NullPointerExeption Si le point spécifié est null
 * @throws IllegalArgumentException Si le coté spécifié est négatif ou null.
 */

private int cote:
private Point CoinSupGauche

public Carre(Point coinSupGauche, int cote) {
  // Appel du constructeur de la classe Rectangle
  super(coinSupGauche,cote,cote);
}
```

```java
/**
 * renvoie le coté du carré
 *
 * @ensures \result > 0;
 * @ensures \result == getLongueur(); ---> renvoi la même valeur
 * @ensures \result == getHauteur();---> renvoi le même valeur
 * @return la valeur du coté de ce carré
 */

public int getCote() {
  return cote;
}


/**
 * Remplacer le coté de ce carré par la valeur absolue specifié
 * Si la valeur absolu est égale à 0, aucune modification n'est apporté à ce carré
 * @ensures (newCote == 0) ==> (getCote()==\old(getCote()));
 * @ensures (newCote > 0) ==> (getCote()== newCote);
 * @ensures (newCote < 0) ==> (getCote()== -newCote);
 */
public setCote(int newCote) {
  setLongueur(Math.obs(newCote));
}

/**
 * Remplace la nouvelle valeur de longueur de ce carre par la valeur specifié
 * Si la valeur spécifié est inférieur ou  ou  ou égale à zéro, ne fait rien et
 * laisse longueur et largeur inchangé.
 *
 * @ensures (nouvelleValeur > 0) ==> (getLongueur()1 == nouvelleValeur) &&
 * (getHauteur == nouvelleValeur) && (getCote == nouvelleValeur))
 *
 * @ensures (nouvelleValeur <= 0) ==> ((getLongueur() == \old(getLongueur())))&&
 * getHauteur()==\old(getHauteur()) && getCote == \old(getCote)
 *
 * @param nouvelleValeur nouvelle valeur de longueur de la longueur du carré.
 */
 @Override
 public void setLongueur(int nouvelleValeur) {
   super.setLongueur(nouvelleValeur);
   super.setHauteur(nouvelleValeur);
 }

 public void setHauteur(int nouvelleValeur) {
   super.setLongueur(nouvelleValeur);
 }
```
