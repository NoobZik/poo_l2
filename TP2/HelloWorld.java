/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HelloWorld.java                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/13 20:15:26 by NoobZik           #+#    #+#             */
/*   Updated: 2017/10/05 13:51:33 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/**
 * Ce fichier ne fais pas partie du TD/TP
 * C'est juste un teste
 */

/**
 * Classe HelloWorld qui permet d'afficher HelloWorld
 */
public class HelloWorld {
  public static void main(String[] args) {
    int[] array;
    array = new int[10];
    int i = -1;
    fillArray(array);
    showArray(array);
  }

  public static void fillArray(int[] array){
    int i = 0;
    while(i < 10){
      array[i] = i;
      i++;
    }
  }

  public static void showArray(int[] array){
    int i = 0;
    while(i < 10){
      System.out.println(array[i]);
      i++;
    }
  }
}
