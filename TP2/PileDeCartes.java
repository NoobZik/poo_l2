/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PileDeCartes.java                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/13 22:30:19 by NoobZik           #+#    #+#             */
/*   Updated: 2017/09/27 19:49:04 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

public class PileDeCartes{
  private Carte[] tabCartes;
  private int     nbCartes;
  private boolean faceVisible;

  /**
  * Contrusteur PileDeCartes
  *
  * initialise le tableau à 53, le nombre de cartes est de 0 et il ne sont
  * pas visible
  */
  public PileDeCartes(){
    this.tabCartes = new Carte[53];
    this.nbCartes = 0;
    this.faceVisible = false;
  }

  /**
   * This function is returning a boolean if the card is flipped or nah
   * @return boolean TRUE or FALSE
   */
  public boolean getFaceVisible() {
    return this.faceVisible;
  }

  /**
   * Filling an Empty Reserve with an ordered
   */
  public void fillEmptyReserve(){
    int i = 1,
        j = 1;

  while(i < 53){
      while(j <= 13){
        if(i <= 13)
          this.tabCartes[i] = new Carte("Trefle", j);
        if(i > 13 && i <= 26)
          this.tabCartes[i] = new Carte("Carreau", j);
        if(i > 26 && i <= 39)
          this.tabCartes[i] = new Carte("Pic", j);
        if(i > 39 && i <= 52)
          this.tabCartes[i] = new Carte("Coeur", j);
        j++;
        i++;
        }
      j = 1;
    }
    this.nbCartes = i-1;
  }

  /**
   * Change the state of visible card
   */
  public void applyFaceVisible() {
    if(getFaceVisible())
      this.faceVisible = false;
    else
      this.faceVisible = true;
  }

  /**
   * Retrive the current amount of card
   * @return int nbCartes
   */
  public int getNbCarte(){
    return this.nbCartes;
  }

  /**
   * For debugging purspose, check the current state of the pile
   */
  public void displayReserve(){
    int i = 0;
    if(this.nbCartes != 0)
      while(i++ < getNbCarte()){
        this.tabCartes[i].showCarte();
      }
    else
      System.out.println("Aucune carte dans la reserve");
  }

  /**
   * Display the first card of the pile
   */
  public void displayFirstCard(){
    this.tabCartes[0].showCarte();
  }
}
