/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Jeu.java                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/13 20:45:08 by NoobZik           #+#    #+#             */
/*   Updated: 2017/09/20 18:20:17 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

class Jeu {
  public static void main(String[] args) {

    private Carte uneCarte, uneCarte_2;
    private PileDeCartes unePile;

/*    uneCarte    = new Carte("Trefle", 11);
    uneCarte_2  = new Carte();

    unePile = new PileDeCartes();

    uneCarte.showCarte();
    uneCarte_2.showCarte();
*/
    unePile = new PileDeCartes();
    unePile.fillEmptyReserve();
    unePile.displayReserve();
  }

  public void showCardReserve(PileDeCartes unePile){
    if(!unePile.getFaceVisible())
      unePile.displayFirstCard();
  }
}
