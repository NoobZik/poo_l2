# TD de Programmation Orienté Objet #

## Notes : ##

J'ai pratiqué le copié collé du tableau

## Exercice 1 : **Vers la Programmation orienté Objet** ##

### Question 1 ###

2 Modules :
*  Carte / PileDeCarte
     *  Champs / Attribut
     *  Opération

2 Classes :

*  Carte / PileDeCarte
     *   Attribut
     *   Methode

### Question 2 ###

**En C :**

```c
carte *creer_carte(...){
  // Initialisation
  return res;
}

void libererCarte(Carte *carte){
  free(carte);
}
```

**En Java**

```java
Carte uneCarte = new Carte();
                  ^ /* Garbage collector */
```

## Exercice 2 **Premiere classe Java** ##
Classe et Objet veulent dire la même chose

**Carte.java**
* Famille
* Rang

```java
public class Carte {
  public String  famille;
  public int     rang;
}
```

**Jeu.java**

```java
public class Jeu{
  public static void main (String[] args){
    Carte uneCarte = new Carte();
    uneCarte.famille = "Pique";
    uneCarte.rang = 11;
    System.out.println(uneCarte.rang + " de " + uneCarte.famille);
  }


  public void afficher(Carte c){
    String r;
    switch(c.rang){
      case 1 : r = "as";break;
      case 11 : r = "valet"; break;
      case 12 : r = "dame"; break;
      case 13 : r = "roi"; break;
      default : r = " " + c.rang;
    }
  }
}
```

## Exercice 3 : **Les Contrustor** ##

**Carte.java**

```java

public Carte(){
  this.rang = 13;
  this.famille = "Trefle";
}

public Carte (String famille, int rang){
  this.rang = rang;
  this.famille = famille;
}
```

## Exercice 4 : **Visibilité des attribut** ##

On va utiliser les accessurs = *get / set*

```java
public String getFamille() {
  return this.famille;
}

System.out.println(uneCarte.getFamille);
```

```java
public void setFamille(String s){
  this.famille = s;
}
```

```java
public Carte (String f, int r) {
  this.setFamille(f);
  this.setRang(r);
}
```
