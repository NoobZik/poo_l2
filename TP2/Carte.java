/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Carte.java                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/13 20:27:13 by NoobZik           #+#    #+#             */
/*   Updated: 2018/09/26 22:17:16 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/**
  * @author NoobZik
  * @since v1
  * @version check_git
  */

public class Carte {
  private String  famille;
  private int     rang;

  /**
   * Permet de retourner la famille de carte
   *
   * @return String
   */
  public String getFamille(){
    return famille;
  }

  /**
   * Fonction qui sert d'affichage du rang
   */
  public void getRangName(){
    if(rang == 1)
      System.out.print("As de ");
    else if(rang == 11){
      System.out.print("Valet de ");
    }
    else if (rang == 12){
      System.out.print("Dame de ");
    }
    else if (rang == 13){
      System.out.print("Roi de ");
    }
    else
      return;
  }

  /**
   * Fonction qui permet de retrouver le rang de la carte
   */
  public void getRang(){
    if(rang > 1 && rang < 11)
      System.out.printf("%d de ", rang);
    else {
      getRangName();
    }
  }

  /**
   * Contrustor - Default
   * initialise la famille en trefle au rang 13
   */
  public Carte (){
    this.famille  = "Trefle";
    this.rang     = 13;
  }

  /**
   * Contrustor
   *
   * @param  famille       Pic / Trefle / Coeur / Carreau
   * @param  rang          chiffre de 1 � 13
   */
  public Carte (String famille, int rang){
    this.famille  = famille;
    this.rang     = rang;
  }

  /**
   * Affichage de la carte
   */
  public void showCarte(){
    System.out.print("La carte actuelle est : ");
    this.getRang();
    System.out.println(this.getFamille());
  }

  /**
   * Permet de retrouver la couleur de la carte
   *
   * @return retourne 1 si noir, 2 si rouge
   */
  public int getColor() {
    if(this.getFamille().equals("Trefle") || this.getFamille().equals("Pique"))
      return 1;
    else if (this.getFamille().equals("Coeur") || this.getFamille().equals("Carreau"))
      return 2;
    else return 0;
  }
}
