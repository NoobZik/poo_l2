# Programmation Orienté Objet TP 2 #

## Descriptif ##

Institut Galilée - Licence 2 Informatique
Exercice effectué par Rakib Sheikh "NoobZik"
Cours : Programmation Orienté Objet


Tout les TP sont suivit par un gestion de controle (Git)


## Notes pour ce TP / TD ##


Je vais peut etre utiliser un makefile pour eviter de tout retapper les sh de
compilations...

Les solutions du TD devrait se trouver dans le fichier Solutions.md

Il doit avoir des fichiers qui servent à rien aussi...
Les profs ont la facheuse idée d'utiliser un Switch Case (J'essaye d'appliquer la norme 42 au maximum !).


***

## Execution du programme ##


Pour compiler


```bash!

$ javac Jeu.java
$ java Jeu

```

Pour generer une documentation


```bash!
$ javadoc -d doc *.java
```
* *javadoc* C'est le compilateur de docs
* -d permet de spécifier le dossier du doc
* doc c'est le nom du dossier (donc ca sera ./docs)
* \*.java c'est pour tout les fichiers java.
