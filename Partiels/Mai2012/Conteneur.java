package examMai2012;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * <p>
 * Cette classe abstraite fournit l'interface et une implémentation partielle
 * d'un conteneur d'éléments. Cette implémentation est indépendante de la
 * structure de données utilisée pour mémoriser les éléments présents dans un
 * tel conteneur, elle est destinée à simplifier la tâche du programmeur
 * souhaitant donner une nouvelle implémentation de cette classe.
 * </p>
 * <p>
 * Les principales caractéristique d'un <code>Conteneur</code> sont :
 * <ul>
 * <li>les éléments contenus ne sont pas ordonnés ;</li>
 * <li>un <code>Conteneur</code> peut contenir plusieurs exemplaires d'un même
 * élément (au sens de <code>equals</code>) ;</li>
 * <li>un <code>Conteneur</code> ne peut pas contenir la valeur
 * <code>null</code>.</li>
 * </ul>
 * </p>
 * 
 * 
 * @invariant getNbObjets() >= 0;
 * @invariant estVide() <==> getNbObjets() == 0;
 * @invariant !contient(null);
 * @invariant (\forall E elt; ; contient(elt) <==> (contientCombienDe(elt) > 0));
 * 
 * @author Marc Champesme
 * @version 12 mai 2012
 * @since 12 mai 2012
 */
public abstract class Conteneur<E> implements Iterable<E>, Cloneable {

	/**
	 * Ajoute l'élément specifié à ce <code>Conteneur</code>. Si l'élément est
	 * déjà présent, un exemplaire supplémentaire de cet élément y est ajouté.
	 * La présence d'un élément est testée en utilisant la methode
	 * <code>equals</code>. L'argument doit être non <code>null</code>.
	 * 
	 * @requires elt != null;
	 * @ensures contient(elt);
	 * @ensures contientCombienDe(elt) == \old(contientCombienDe(elt)) + 1;
	 * 
	 * @param elt
	 *            L'élément à ajouter dans ce <code>Conteneur</code>
	 * 
	 * @throws NullPointerException
	 *             si l'élément spécifié est <code>null</code>.
	 */
	public abstract void ajouter(E elt);

	/**
	 * Ajoute <code>nCopies</code> exemplaires de l'élément specifié à ce
	 * <code>Conteneur</code>. Si l'élément est déjà présent,
	 * <code>nCopies</code> exemplaires supplémentaires de cet élément y sont
	 * ajoutés. La présence d'un élément est testée en utilisant la methode
	 * <code>equals</code>.
	 * <p>
	 * L'élément doit être non <code>null</code> et <code>nCopies</code> doit
	 * être strictement positif.
	 * </p>
	 * 
	 * @requires nCopies > 0;
	 * @requires elt != null;
	 * @ensures contient(elt);
	 * @ensures contientCombienDe(elt) == \old(contientCombienDe(elt)) + nCopies;
	 * 
	 * @param elt
	 *            L'élément à ajouter dans ce <code>Conteneur</code>
	 * @param nCopies
	 *            Le nombre d'exemplaires de l'élément à ajouter à ce
	 *            <code>Conteneur</code>.
	 * 
	 * @throws IllegalArgumentException
	 *             si <code>nCopies</code> est inférieur ou égal à 0.
	 * 
	 * @throws NullPointerException
	 *             si l'élément spécifié est <code>null</code>.
	 */
	public void ajouter(E elt, int nCopies) {
		if (elt == null) {
			throw new NullPointerException(
					"L'élément ajouté doit être non null.");
		}

		if (nCopies <= 0) {
			throw new IllegalArgumentException(
					"Le nombre d'occurences de l'élément"
							+ "à ajouter doit être strictement positif.");
		}

		for (int i = 1; i <= nCopies; i++) {
			ajouter(elt);
		}
	}

	/**
	 * Renvoie <code>true</code> si ce <code>Conteneur</code> contient au moins
	 * un exemplaire de l'élément specifié. La présence d'un élément est testée
	 * en utilisant la méthode <code>equals</code>.
	 * 
	 * @ensures \result <==> contientCombienDe(obj) > 0;
	 * @ensures estVide() ==> !\result;
	 * 
	 * @param obj
	 *            élément dont on cherche à savoir s'il est présent dans ce
	 *            <code>Conteneur</code>.
	 * @return <code>true</code> si ce <code>Conteneur</code> contient au moins
	 *         un exemplaire de l'élément specifié ; <code>false</code> sinon.
	 */
	public boolean contient(Object obj) {
		for (E elt : this) {
			if (elt.equals(obj)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Renvoie le nombre d'exemplaires de l'élément specifié présents dans ce
	 * <code>Conteneur</code>. La présence d'un élément est testée en utilisant
	 * la méthode <code>equals</code>.
	 * 
	 * @ensures \result >= 0;
	 * @ensures contient(obj) <==> \result > 0;
	 * @ensures !contient(obj) <==> \result == 0;
	 * @ensures \result <= getNbObjets();
	 * 
	 * @param obj
	 *            élément dont on cherche à connaitre le nombre d'exemplaires
	 *            présents dans ce <code>Conteneur</code>.
	 * @return le nombre d'exemplaires de l'élément specifié présents dans ce
	 *         <code>Conteneur</code>.
	 */
	public int contientCombienDe(Object obj) {
		int cpt = 0;
		for (E elt : this) {
			if (elt.equals(obj)) {
				cpt++;
			}
		}
		return cpt;
	}

	/**
	 * Renvoie <code>true</code> si ce <code>Conteneur</code> ne contient aucun
	 * élément.
	 * 
	 * @ensures \result <==> (getNbObjets() == 0);
	 * 
	 * @return <code>true</code> si ce <code>Conteneur</code> ne contient aucun
	 *         élément ; <code>false</code> sinon.
	 */
	public boolean estVide() {
		return getNbObjets() == 0;
	}

	/**
	 * Renvoie le nombre d'éléments présents dans ce <code>Conteneur</code>.
	 * Chaque occurence de chaque élément est comptée.
	 * 
	 * @ensures \result >= 0;
	 * 
	 * @return Le nombre d'éléments dans ce <code>Conteneur</code>.
	 */
	public abstract int getNbObjets();

	/**
	 * Renvoie un itérateur sur les éléments de ce <code>Conteneur</code>.
	 * 
	 * @ensures \result != null;
	 * 
	 * @return Un itérateur sur les éléments de ce <code>Conteneur</code>.
	 */
	public abstract Iterator<E> iterator();

	/**
	 * Renvoie une nouvelle liste (i.e. une nouvelle instance d'une classe
	 * implémentant l'interface <code>List</code>) contenant les mêmes éléments
	 * en autant d'exemplaires que ce <code>Conteneur</code>.
	 * 
	 * @ensures \result != null;
	 * @ensures \result.size() == this.getNbObjets();
	 * @ensures (forall E elt; this.contient(elt); \result.contains(elt));
	 * 
	 * @return une nouvelle liste contenant les mêmes éléments que ce
	 *         <code>Conteneur</code>.
	 */
	public List<E> asList() {
		List<E> result = new LinkedList<E>();
		for (E elt : this) {
			result.add(elt);
		}
		return result;
	}

	/**
	 * Retire un exemplaire de l'élément specifié de ce <code>Conteneur</code>
	 * si cet élément y est présent. Renvoie <code>true</code> si cet élément
	 * était effectivement présent dans ce <code>Conteneur</code> et que
	 * l'élément a pu être effectivement retiré ; renvoie <code>false</code>
	 * sinon. La présence d'un élément est testée en utilisant la méthode
	 * <code>equals</code>. Renvoie <code>false</code> si l'argument est
	 * <code>null</code>.
	 * 
	 * @ensures (elt == null) ==> !\result;
	 * @ensures \old(contient(elt)) <==> \result;
	 * @ensures \old(contient(elt)) 
	 *       <==> (contientCombienDe(elt) == \old(contientCombienDe(elt)) - 1);
	 * @ensures \old(contientCombienDe(elt) <= 1) <==> !contient(elt);
	 * @ensures \old(contientCombienDe(elt) > 1) <==> contient(elt);
	 * 
	 * @param elt
	 *            élément dont un exemplaire doit être retirer de ce
	 *            <code>Conteneur</code>
	 * @return <code>true</code> si cet élément etait effectivement present ;
	 *         <code>false</code> sinon.
	 */
	public abstract boolean retirer(Object elt);

	/**
	 * Retire <code>nCopies</code> exemplaires de l'élément specifié de ce
	 * <code>Conteneur</code> si cet élément y est présent. Renvoie le nombre
	 * d'exemplaires de l'élément effectivement retirés. La présence d'un
	 * élément est testée en utilisant la méthode <code>equals</code>.
	 * <code>nCopies</code> doit être supérieur ou égal à 0.
	 * 
	 * @requires nCopies >= 0;
	 * @ensures elt == null ==> \result == 0;
	 * @ensures \old(contient(elt)) && nCopies > 0 <==> \result > 0;
	 * @ensures \result == (\old(contientCombienDe(elt))
	 *            - contientCombienDe(elt));
	 * @ensures contientCombienDe(elt) == Math.max(0,
	 *                             \old(contientCombienDe(elt)) - nCopies);
	 * @ensures contientCombienDe(elt) == (\old(contientCombienDe(elt))
	 *                             - \result);
	 * @ensures \old(contientCombienDe(elt) <= nCopies) ==> !contient(elt);
	 * @ensures \old(contientCombienDe(elt) > nCopies) <==> contient(elt);
	 * 
	 * @param elt
	 *            élément dont un exemplaire doit être retirer de ce
	 *            <code>Conteneur</code>.
	 * @param nCopies
	 *            Nombre d'exemplaires de l'élément spécifié à retirer de ce
	 *            <code>Conteneur</code>.
	 * @return Le nombre d'exemplaires de l'élément effectivement retirés de ce
	 *         <code>Conteneur</code>.
	 * @throws IllegalArgumentException
	 *             si <code>nCopies</code> est strictement négatif
	 */
	public int retirer(Object elt, int nCopies) {
		if (nCopies < 0) {
			throw new IllegalArgumentException(
					"Le nombre d'occurences de l'élément"
							+ "à retirer doit être positif ou nul.");
		}
		if (elt == null || nCopies == 0) {
			return 0;
		}
		int nbRetraits = 0;
		boolean trouve = true;
		while (nbRetraits < nCopies && trouve) {
			trouve = retirer(elt);
			if (trouve) {
				nbRetraits++;
			}
		}

		return nbRetraits;
	}

	/**
	 * Retire de ce <code>Conteneur</code> tous les exemplaires de l'élément
	 * spécifié qu'il contient.
	 * 
	 * @ensures \result >= 0;
	 * @ensures \result == 0 <==> !\old(contient(elt));
	 * @ensures \result == \old(contientCombienDe(elt));
	 * 
	 * @param elt
	 *            L'élément à retirer de ce <code>Conteneur</code>.
	 * 
	 * @return Le nombre d'exemplaires de l'élément spécifié effectivement
	 *         retirer de ce <code>Conteneur</code>.
	 */
	public int retirerTout(Object elt) {
		return retirer(elt, getNbObjets());
	}

	/**
	 * Retire de ce <code>Conteneur</code> tous les éléments qu'il contient.
	 * 
	 * @ensures estVide();
	 */
	public abstract void vider();

	/**
	 * <p>
	 * Teste si l'élément spécifié est <code>equals</code> à ce
	 * <code>Conteneur</code>. Ce <code>Conteneur</code> et l'élément spécifié
	 * sont <code>equals</code> si et seulement si l'élément spécifié est un
	 * <code>Conteneur</code> contenant les mêmes éléments en autant
	 * d'exemplaires.
	 * </p>
	 * 
	 * @ensures (* contrat Object.equals(): symétrie *)
	 * o != null ==> (\result <==> o.equals(this));
	 * @ensures (* contrat Object.hashCode() *)
	 * \result ==> (this.hashCode() == o.hashCode());
	 * @ensures !(o instanceof Conteneur<?>) ==> !\result;
	 * @ensures \result ==> (getNbObjets() == ((Conteneur<?>) o).getNbObjets());
	 * @ensures (o instanceof Conteneur<?>)
	 *      ==> (\result 
	 *           <==> ((getNbObjets() == ((Conteneur<?>) o).getNbObjets()) 
	 *                 && (\forall E elt; contient(elt);
	 *                       this.contientCombienDe(elt)
	 *                       == ((Conteneur<?>) o).contientCombienDe(elt))));
	 * 
	 * @param o
	 *            L'élément à comparer à ce <code>Conteneur</code>.
	 * @return <code>true</code> si et seulement si l'élément spécifié est un
	 *         <code>Conteneur</code> contenant les mêmes éléments en autant
	 *         d'exemplaires ; <code>false</code>. sinon.
	 * @see Object#equals(Object)
	 */
	public boolean equals(Object o) {
		if (!(o instanceof Conteneur<?>)) {
			return false;
		}
		Conteneur<?> c = (Conteneur<?>) o;
		if (this.getNbObjets() != c.getNbObjets()) {
			return false;
		}
		List<E> reste = this.asList();
		reste.removeAll(c.asList());
		return reste.isEmpty();
	}

	/**
	 * Renvoie un code de hashage pour ce <code>Conteneur</code>.
	 * L'implémentation fournie par cette classe respecte le contrat défini dans
	 * la classe <code>Object</code> relativement à l'implémentation de
	 * <code>equals</code> fournie par cette classe.
	 * 
	 * @return Un code de hashage pour ce <code>Conteneur</code>.
	 */
	public int hashCode() {
		int code = 0;
		for (E elt : this) {
			code += elt.hashCode();
		}
		return code;
	}

	/**
	 * Renvoie une représentation succincte de ce <code>Conteneur</code> sous
	 * forme de chaîne de caractères.
	 * 
	 * @return une représentation succincte de ce <code>Conteneur</code>.
	 */
	public String toString() {
		String result = "(";
		boolean isFirstElt = true;
		for (E elt : this) {
			if (!isFirstElt) {
				result = result + ", ";
				isFirstElt = false;
			}
			result = result + elt.toString();
		}
		return result + ")";
	}
}
