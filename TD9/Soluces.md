# Exercice 1 #

1.  La classe Piece contient des sorties, et il est possible d'ajouter et de retirer
des objets.
*   La classe Joueur possède un poids maximum, et il est possible d'ajouter et de
 retirer des obets.

2.
```java
public ObjetZork[] getTabObjet();
public int getNbObjets();
public boolean contient(ObjetZork oz);
public int contientCombienDe(ObjetZork oz);
public void ajouter(ObjetZork oz);
public boolean retirer(ObjetZork oz);
```

Tout ces signatures (ou protortype) des méthodes en commun Classe Joueur et Piece.

3.
La méthode "ajouter" pose un problème car pour un joueur cette methode doit
vérifier le poids maximum du joueur à ne pas dépasser. Il y a donc une
contrainte à vérifier.

**Remarque** Pour définir la méthode "ajouter", on aura la même définition, seul
 une contrainte change la classe Joueur. On garde donc une méthode "ajouter" en
 introduisant une nouvelle méthode pour tester la constrainte d'ajout.
 Cette methode sera :
```java
boolean ajoutPossible(ObjetZork oz);
```
Dans ce cas, la classe Piece, cette méthode renvoie toujours "true", mais elle
ne renvoie pas dans la classe Joueur, que si le poids maximum n'est pas dépassé.

Ainsi dans la méthode "ajouter", il suffit de tester ajoutPossible avant
d'éffectuer un ajout. Cette astuce permet de garder la methode "ajouter" commune
aux deux classes.
La methode ajoutPossible doit avoir une implémentation différente dans chaque
classe (Piece et Joueur) et doit être abstract dans la super classe.
Le prototype et donc : public abstract boolean ajoutPossible(ObjetZork oz)

**Remarque :** On peut définir la super Classe comme étant concrête, il faudra
dans ce cas définir ajoutPossible (Elle renvoie true) et la redéfinir dans la
classe joueur. La super classe est donc abstraite.  

4.
```java
/**
 * La classe ArrayListe utilisant un arrayList comme struct de données
 */
public abstract class ArrayListConteneur() {
  /**
   * Initialise un nouveau conteneur vide.
   * @ ensures gtNbObjet() == 0
   */
  public ArrayListConteneur() {

  }
  /**
   * On Initialise une nouvelle instance contenant les ObjetZork contenu dans
   * la liste spécifié, le paramètre doit être non <code> NULL </code>
   * et tout les elements spécifiés doivent être no null
   * @param objet liste des ObjetZork à la place de ce conteneur.
   * @throws NullPointerExeption si le paramètre est <code>null</code
   *  ou l'un des éléments de la liste est <code>null</null>
   */
  /*@
    @ requires (\forall int i; i>=0 && i < object.size(); objet.get(i) != null);
    @ ensures (\forall int i; i > 0 && i < object.size(); contient(objet.get(i)));
    @*/

    public ArrayListConteneur (List<ObjetZork> objets) {

    }

    public int contientCombienDe(ObjetZork oz);

    public boolean contien(ObjetZork oz);

    public int getNbObjet();

    pubic ObjetZork[] getIntObjets() {}

    public void ajouter(ObjetZork oz) {}

    public boolean retirer(ObjetZork oz) {}

    public boolean newConteneur(Conteneur c) {}

    public abstract boolean ajoutPossible(ObjetZork oz) {}

    /**
     * indique si l'objet est extends peut etre ajouter dans le conteneur
     * @param oz l'ObjetZork
     * @puure
     */

}
