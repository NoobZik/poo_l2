# Semaine du 15 octobre 2018 TD #

## Exercice 1 ##

1.  Le contenue de la liste aux trois étapes :
Etape 1.

```java
al=[parachemin1,sabre,parachemin1]
```

Etape 2 : Le contrat de la methode remove est très clare, c'est la premire occurrence qui est retiré de la liste est on clone:

```java
 al=[sabre,parachemin1] : // L'élement à suuprimer ayant été trouvé, sa premiere occurrence est retiré;
```
Etape 3 :

```java
 al=[sabre] // l'élément supprimer ayant été trouvé dans la liste, "secondRemove" prend la valeur de true
```

2.  Dans cette question on suppose que la class ObjetZork ne redéfinit pas la methode "equals". On cherche donc a retirer un element de la liste (l'élement à supprimer est l'argument de la methode remove).

Etape 1 : \[parachemin1, sabre, parachemin1\]
Etape 2 : Le seul élement de la liste == à l'argument de remove etant parachemin1; c'est cet élément qui est retiré de la liste est on a :

```java
al = [sabre,parachemin1]
```
est "firstRemove" prends la valeur true.

Etape 3 : aucun élément de la liste == a l'argument de remove, la liste reste inchangé
```java
al=[sabre, parachemin1], //l'élément n'ayant pas été trouvé "secondRemove prends la valeur de false"
```

3.  Un procédé possible si "equals" est redéfinit par ObjetZork consiste à repeter l'execution de l'operation remove (jusqu'a ce que la méthode renvoie false)

```java
ArrayList<ObjetZork> al = new ArrayList<ObjetZork>();
ObjetZork oz = new ObjetZork("parachemin1",1);
al.add(oz);
boolean removeEffectif;
do {
  removeEffectif = al.remove(oz);
}while (!removeEffectif);
```

ou bien
```java
while(al.remove(oz)){}
```

4.  l'execution de la premiere instruction (al.set()) provoque l'interruption de l'execution par une exception "IndexOutBoundsException" contrairement à un tableau. Un ArrayList ne permet qu'aux indexes ou un element est deja présent, dans cet exemple, l'index fournit par l'opération, set est 5, il doit être impérativement < 3 car la liste contient que 3 éléments.
Le contrat de cette méthode inclus une précondition qui est exprimée dans la documentation par la classe throws:"IndexOutBoundsException-if the index out of range (index <0 || inde => size());"

5.  Ce code permet d'ajouter 20 éléments à la liste initiale, dont le contenue sera

```java
al=[parachemin1,sabre,parachemin1,"rocher":poids20,"rocher":poids21,...,"rocher":poids31]
```
La taille de la liste sera 23, mais la valeur exacte de sa capacité ne peut être connu avec certidude, nous pouvons être assurer que taille <= capacité (cd ("\[theCapacity]")) is always at least as large as the list size) comme mentionné dans la documentation pour éléments on utilise un tableau.


tableau pour memoriser les elements de la liste structure interne pour memoriser les elements d'un ArrayList. la taille du tableau étant définitivement fixer lors de sa création. lorsque la capacité est insuffisante, un tableau de taille plus grande est alloué. les elements de la liste y sont copies puis ce nouveau tableau se substitue au tableau précédent.
Cette opération de reallocation est trop couteuse en temps d'éexecution . La solution serait que l'utilisateur peut anticiper sur la capacité de l'arraylist par la methode ensureCapacity qui va permettre d'allouer un tableay de taille suffisante et d'éviter les multiples reallocation possibles
