# TD/TP de Programmation Orientée Objet #

## Definition des classes ##

Warning, ce sont juste des idées évoqué en TD par les autres

### Exercice 1 ###

Pour la defnition d'un objet zork, ils veulent :
*   La description de l'objet
*   La piece dans laquelle où il se trouve
*   Poids
*   Transportable ou pas
*   Position de l'objet dans la piece


```java
public class ObjetZork {
  public ObjetZork (String d, int p, boolean c, String pos) {
    this.description = d;
    this.poids = p;
    this.carryable = true;
    this.position = pos;

  }
}

```
